const btnLimpiar = document.getElementById("btnLimpiar");
const email = document.getElementById("correo");
const pass = document.getElementById("pass");

const limpiar = () => {
  pass.value = "";
  email.value = "";
};

btnLimpiar.addEventListener("click", limpiar);

// Import the functions you need from the SDKs you need
import {
  initializeApp
} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import {
  signInWithEmailAndPassword,
  getAuth,
} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

// Your web app's Firebase configuration

const firebaseConfig = {
  apiKey: "AIzaSyDH8awOpgeyM21Lrj-y2gq7K-yEXJ8HOrg",
  authDomain: "webfinal-5d26e.firebaseapp.com",
  databaseURL: "https://webfinal-5d26e-default-rtdb.firebaseio.com/",
  projectId: "webfinal-5d26e",
  storageBucket: "webfinal-5d26e.appspot.com",
  messagingSenderId: "271048248832",
  appId: "1:271048248832:web:4c184e46f91620161ad2e5"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

window.onload = inicializar;
var formAutentificacion;

function inicializar() {
  formAutentificacion = document.getElementById("form--autentificacion");
  formAutentificacion.addEventListener("submit", autentificar);
}

async function autentificar(event) {
  event.preventDefault();

  const email = event.target.correo.value;
  const password = event.target.pass.value;

  const auth = await getAuth();
  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      const user = userCredential.user;
      alert("Bienvenido al administrador");
      window.location.href = "/html/administrador.html";
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      alert("Error Email o Password Incorrectos");
    });
}