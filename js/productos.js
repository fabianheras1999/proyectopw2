// Import the functions you need from the SDKs you need
import {
  initializeApp
} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import {
  getDatabase,
  onValue,
  ref,
} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDH8awOpgeyM21Lrj-y2gq7K-yEXJ8HOrg",
  authDomain: "webfinal-5d26e.firebaseapp.com",
  projectId: "webfinal-5d26e",
  storageBucket: "webfinal-5d26e.appspot.com",
  messagingSenderId: "271048248832",
  appId: "1:271048248832:web:4c184e46f91620161ad2e5"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const contenedorProductos = document.getElementById("contenedor--productos");
const templateProduct = document.getElementById("templateProduct");

function cardProducts(nombre, precio, editorial, fecha, url) {
  let template = templateProduct.content.cloneNode(true);
  let imagenProducto = template.querySelector("img");
  let nombreProducto = template.querySelector("h3");
  let precioProducto = template.querySelector("span.producto--precio");
  let descripcion = template.querySelector("span.producto--descripcion");

  imagenProducto.src = url;
  nombreProducto.innerHTML = nombre + "<hr/>";
  precioProducto.innerHTML = "$" + parseFloat(precio).toFixed(2) + " MXN";
  descripcion.innerHTML =
    "<br/>" + "Año de Lanzamiento: " + fecha;

  contenedorProductos.appendChild(template);
}

async function mostrarProductos() {
  const dbRef = ref(db, "productos");

  await onValue(
    dbRef,
    (snapshot) => {
      contenedorProductos.innerHTML = "";
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val();

        if (childData.status === 0) {
          cardProducts(
            childData.nombre,
            childData.precio,
            childData.editorial,
            childData.fecha,
            childData.imagen
          );
        }

        console.log(childKey + ":");
        console.log(childData.nombre);
      });
    }, {
      onlyOnce: true,
    }
  );
}

mostrarProductos();